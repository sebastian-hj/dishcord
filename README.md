# diSHcord

## Description

Discord bot from SH (`diSHcord`) for miscellaneous uses.

## Set-up

1. Install Python development tools and virtual environment utilities:
    ```
    sudo apt install python3-venv python3-dev
    ```
1. Install Python virtual environment for Python 3.7+ with system site packages:
    ```
    python3 -m venv --system-site-packages .venv
    ```
    - System site packages are needed as [picamera2](https://github.com/raspberrypi/picamera2) library cannot (yet) be installed with `pip` and it is part of the Raspberry Pi OS system
1. Enable virtual environment:
    ```
    source .venv/bin/activate
    ```
1. Upgrade `pip`:
    ```
    (.venv) pip install --upgrade pip
    ```
1. Install `dishcord` in editable mode:
    ```
    (.venv) pip install -e .
    ```

    or in normal mode:
    ```
    (.venv) pip install .
    

## Configuration File

A JSON configuration file is required as an argument to `diSHcord` script to provide Discord token:
```json
    "discord": {
        "token": ""
    }
```

## Commands

All commands must be prefixed with `/`. Supported commands are:
- `hello`: Reply with hello

## Bots

### Chloé Cam

A simple bot to take photo with Raspberry Pi camera.

#### Commands

- `/photo`: Take photo

### shReddit

Reddit bot from SH (`shReddit`) based on `praw` to parse specific Reddit posts.

#### Configuration File

Fields required for `shReddit` bot:
```json
    "reddit": {
        "client_id": "",
        "client_secret": "",
        "username": "",
        "password": "",
        "user_agent": "",
        "subreddit": [
            {
                "name": "analog",
                "type": "hot",
                "posts": 5
            }
        ],
        "schedule_time": ["8:00", "12:00", "18:00"]
    }
```

#### Usage

For daily times specified on `schedule_time` from configuration file, `shReddit` will send `subreddit.posts` number of messages for subreddit `subreddit.name` filtered with type `subreddit.type`.
NOTE: not yet working :/


#### Commands

- `/reddit`: Trigger messages sending `subreddit.posts` number of messages for subreddit `subreddit.name` filtered with type `subreddit.type`
