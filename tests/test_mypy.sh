#!/bin/bash
set -ueo pipefail

# Activate virtual environment
source .venv/bin/activate

# Run mypy on Discord Bot
mypy ./dishcord_bot.py

# Run mypy on dishcord cogs
# mypy ./src/dishcord/cogs

# Run mypy on dishcord modules
mypy ./src/dishcord/modules/

# Run mypy on tests/
mypy ./tests/
