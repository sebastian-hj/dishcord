import json
from pathlib import Path
from typing import Generator

import pytest

from dishcord.modules.shreddit import ShReddit


def read_file_to_json_dict(file_path: Path) -> dict:
    json_dict: dict
    try:
        with open(file_path, "r") as f_id:
            json_dict = json.load(f_id)
    except IOError:
        print(f"ERROR: cannot open file '{file_path}'")
        json_dict = dict()
    except ValueError:
        print(f"ERROR: cannot decode file '{file_path}'")
        json_dict = dict()

    return json_dict


@pytest.fixture(scope="class")
def set_up() -> Generator[ShReddit, None, None]:
    config = read_file_to_json_dict(Path(__file__).parent / ".." / "config_sh.json")

    shreddit = ShReddit(config["reddit"])

    yield shreddit


class TestShreddit:
    def test_initialize(self, set_up: ShReddit) -> None:
        shreddit = set_up

        assert shreddit is not None

    def test_message(self, set_up: ShReddit) -> None:
        shreddit = set_up

        message = shreddit.create_reddit_message()

        assert message

        print(message)


if __name__ == "__main__":
    pytest.main([__file__, "-s"])
