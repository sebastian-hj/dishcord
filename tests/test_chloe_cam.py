from typing import Generator

import pytest

from dishcord.modules.chloe_cam import ChloeCam


@pytest.fixture(scope="class")
def set_up() -> Generator[ChloeCam, None, None]:
    config: dict = dict()

    chloe_cam = ChloeCam(config)

    yield chloe_cam


class TestChloeCam:
    def test_initialize(self, set_up: ChloeCam) -> None:
        chloe_cam = set_up

        assert chloe_cam is not None

    def test_take_photo(self, set_up: ChloeCam) -> None:
        chloe_cam = set_up

        photo_path = chloe_cam.take_photo()

        assert photo_path.exists()

    def test_remove_photo(self, set_up: ChloeCam) -> None:
        chloe_cam = set_up

        photo_path = chloe_cam.photo_path

        chloe_cam.remove_photo()

        assert not photo_path.exists()


if __name__ == "__main__":
    pytest.main([__file__, "-s"])
