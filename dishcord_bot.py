import argparse
import asyncio
import json
from pathlib import Path

import discord

from dishcord import ShBot


def read_file_to_json_dict(file_path: Path) -> dict:
    json_dict: dict
    try:
        with open(file_path, "r") as f_id:
            json_dict = json.load(f_id)
    except IOError:
        print(f"ERROR: cannot open file '{file_path}'")
        json_dict = dict()
    except ValueError:
        print(f"ERROR: cannot decode file '{file_path}'")
        json_dict = dict()

    return json_dict


async def main() -> None:
    # Create parser
    parser = argparse.ArgumentParser(description="diSHcord bot")
    parser.add_argument("config_json", help="Path to configuration JSON", type=Path)
    # Get parser results
    args = parser.parse_args()

    # Read file to JSON dictionary
    config = read_file_to_json_dict(args.config_json)
    if not config:
        exit(-1)

    # Discord bot
    intents = discord.Intents.default()
    intents.message_content = True

    async with ShBot(
        command_prefix="/",
        intents=intents,
        config=config,
    ) as bot:
        await bot.start(config["discord"]["token"])


if __name__ == "__main__":
    asyncio.run(main())
