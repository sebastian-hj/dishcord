import collections
import datetime

import praw


class ShReddit:
    def __init__(self, config: dict) -> None:
        self.config = config

        # Start a reddit instance
        self.reddit = praw.Reddit(
            client_id=self.config["client_id"],
            client_secret=self.config["client_secret"],
            username=self.config["username"],
            password=self.config["password"],
            user_agent=self.config["user_agent"],
            check_for_async=False,
        )
        # TODO: Update for multiple subreddits
        self.submission_ids: collections.deque = collections.deque(
            maxlen=config["subreddit"][0]["posts"] * len(config["schedule_time"])
        )

    @staticmethod
    def _get_hour(time_str: str) -> int:
        time_object = datetime.datetime.strptime(time_str, "%H:%M")
        return time_object.hour

    @staticmethod
    def _get_minutes(time_str: str) -> int:
        time_object = datetime.datetime.strptime(time_str, "%H:%M")
        return time_object.minute

    def get_schedule_time_list(self) -> list:
        # Get local timezone
        local_timezone = (
            datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
        )

        if "schedule_time" in self.config:
            # Convert to 'datetime.time' format
            schedule_list = [
                datetime.time(
                    hour=self._get_hour(t),
                    minute=self._get_minutes(t),
                    tzinfo=local_timezone,
                )
                for t in self.config["schedule_time"]
            ]
        else:
            print("ERROR: No 'schedule' field in JSON file!")
            schedule_list = list()

        return schedule_list

    def create_reddit_message(self) -> list[str]:
        # Message
        message_list = list()

        # Iterate over subreddits specified in JSON
        for element in self.config["subreddit"]:
            # Get variables for current subreddit
            subreddit_name = element["name"]
            subreddit_type = element["type"]
            subreddit_number_of_posts = element["posts"]

            # Get posts for subreddit type
            if subreddit_type == "hot":
                subreddit = self.reddit.subreddit(subreddit_name).hot()
            elif subreddit_type == "new":
                subreddit = self.reddit.subreddit(subreddit_name).new()
            elif subreddit_type == "controversial":
                subreddit = self.reddit.subreddit(subreddit_name).controversial()
            elif subreddit_type == "top":
                subreddit = self.reddit.subreddit(subreddit_name).top()
            elif subreddit_type == "rising":
                subreddit = self.reddit.subreddit(subreddit_name).rising()
            else:
                subreddit = None
                message_list.append(
                    f"Sorry, '{subreddit_type}' type not yet supported..."
                )

            if subreddit is not None:
                # Initialize counter
                counter = 0
                # Get posts based on number of posts
                while counter < subreddit_number_of_posts:
                    # Get submission if it is not stickied
                    submission = next(s for s in subreddit if not s.stickied)
                    # Get submission ID
                    submission_id = submission.id
                    # Check if ID has already been used
                    if submission_id not in self.submission_ids:
                        # Append submission ID
                        self.submission_ids.append(submission_id)
                        # Get author name
                        author = (
                            submission.author.name
                            if submission.author is not None
                            else "deleted?"
                        )
                        # Get submission title
                        title = submission.title
                        # Get submission link
                        link = submission.shortlink
                        # Get media type and link
                        media_type = ""
                        media_links = list()
                        if hasattr(submission, "is_gallery"):
                            media_type = "gallery"
                            for x in range(0, len(submission.gallery_data["items"])):
                                # Get media ID in order of gallery
                                media_id = submission.gallery_data["items"][x][
                                    "media_id"
                                ]
                                # Get media link based on media ID order
                                media_links.append(
                                    submission.media_metadata[media_id]["s"]["u"]
                                )
                        elif hasattr(submission, "preview"):
                            media_type = "image"
                            for image in submission.preview["images"]:
                                media_links.append(image["source"]["url"])
                        else:
                            media_type = "text?"
                        # Create media string and construct message but do not generate preview if NSFW
                        if submission.over_18 is True:
                            if media_type == "text?":
                                media_str = ""
                            else:
                                media_str = ""
                                for x in range(0, len(media_links)):
                                    media_str += f"[link{x+1}](<{media_links[x]}>) "
                                media_str = f"{media_str[:-1]}"
                            message = f"----\n[NSFW] '{title}' by /u/{author}: [{media_type}] ( {media_str} ) (<{link}>)"
                        else:
                            if media_type == "text?":
                                media_str = ""
                            else:
                                # Only display fist link; user would need to click on gallery
                                media_str = f"[link]({media_links[0]})"
                            message = f"----\n'{title}' by /u/{author}: [{media_type}] ( {media_str} ) (<{link}>)"
                        # Append message information
                        message_list.append(message)
                    # Increase counter
                    counter += 1

        if not message_list:
            message_list.append("No new posts...")

        return message_list
