import datetime
import os
from pathlib import Path

from picamera2 import Picamera2


class ChloeCam:
    def __init__(self, config: dict) -> None:
        self.config = config

        self.photo_path: Path

        # Initialize
        self.__camera_device = self.__camera_initialize()

    @staticmethod
    def __camera_initialize() -> Picamera2:
        # Start camera
        camera = Picamera2()

        # Configurations
        capture_config = camera.create_still_configuration()
        capture_config["transform"].hflip = True
        capture_config["transform"].vflip = True
        camera.configure(capture_config)

        # Start camera
        camera.start()

        return camera

    def take_photo(self) -> Path:
        timestamp = datetime.datetime.now().strftime("%Y%m%d%A_%H%M%S")

        self.photo_path = Path(__file__).parents[2] / f"{timestamp}.jpeg"

        self.__camera_device.capture_file(self.photo_path)

        return self.photo_path

    def remove_photo(self) -> None:
        self.photo_path.unlink()
