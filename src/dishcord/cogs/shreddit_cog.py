import datetime

import discord
from discord.ext import commands, tasks

from dishcord.modules.shreddit import ShReddit


class ShRedditCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.shreddit = None
        self.shreddit_channel_id = 987885564088954930
        self.shreddit_message.start()

    async def set_config(self, config):
        print("Set config")
        self.shreddit = ShReddit(config)

    # TODO: Would like to use this instead: @tasks.loop(time=shreddit.get_schedule_time_list())
    @tasks.loop(
        time=[
            datetime.time(
                8,
                0,
                tzinfo=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo,
            ),
            datetime.time(
                12,
                0,
                tzinfo=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo,
            ),
            datetime.time(
                18,
                0,
                tzinfo=datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo,
            ),
        ]
    )
    async def shreddit_message(self):
        channel = self.bot.get_channel(self.shreddit_channel_id)
        # print("Working?")
        # await channel.send("Working?")
        reddit_message_list = self.shreddit.create_reddit_message()
        for reddit_message in reddit_message_list:
            try:
                await channel.send(reddit_message)
            except discord.HTTPException as e:
                print(f"ERROR: {e}")
                await channel.send(
                    f"Got the following error while sending the last message: {e}"
                )
                await channel.send("Attempting to send again:")
                await channel.send(f"{reddit_message[:2000]}")

    @shreddit_message.before_loop
    async def before_shreddit_message(self):
        await self.bot.wait_until_ready()


async def setup(bot):
    await bot.add_cog(ShRedditCog(bot))
