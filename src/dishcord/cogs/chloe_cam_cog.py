import discord
from discord.ext import commands

from dishcord.modules.chloe_cam import ChloeCam


class ChloeCamCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.chloe_cam = None
        self.channel_id = 901987669586890804

    async def set_config(self, config):
        self.chloe_cam = ChloeCam(config)

    @commands.command()
    async def photo(self, ctx):
        if ctx.channel.id == self.channel_id:
            await ctx.reply("Taking photo...")
            # Take photo and get photo path
            photo_path = self.chloe_cam.take_photo()
            # Send photo
            await ctx.reply("Here it is!")
            await ctx.reply(file=discord.File(photo_path))
            # Remove photo
            self.chloe_cam.remove_photo()
        else:
            await ctx.reply("Unsupported command in this channel!")


async def setup(bot):
    await bot.add_cog(ChloeCamCog(bot))
