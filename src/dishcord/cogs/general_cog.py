from discord.ext import commands


class GeneralCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def hello(self, ctx):
        await ctx.reply("Hi!")


async def setup(bot):
    await bot.add_cog(GeneralCog(bot))
