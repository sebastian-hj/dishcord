from discord.ext import commands


class ShBot(commands.Bot):
    def __init__(self, *args, config, **kwargs):
        super().__init__(*args, **kwargs)

        # Get parameters
        self.config_discord = config["discord"]
        self.config_reddit = config["reddit"]

    async def setup_hook(self) -> None:
        await self.load_extension(".cogs.general_cog", package="dishcord")

        await self.load_extension(".cogs.shreddit_cog", package="dishcord")
        shreddit_instance = self.get_cog("ShRedditCog")
        await shreddit_instance.set_config(self.config_reddit)

        await self.load_extension(".cogs.chloe_cam_cog", package="dishcord")
        chloe_cam_instance = self.get_cog("ChloeCamCog")
        await chloe_cam_instance.set_config(None)

    async def on_ready(self):
        print(f"Logged in as {self.user} (ID: {self.user.id})")
        print("------")
